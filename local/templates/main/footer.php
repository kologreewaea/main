<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

</main>
<footer class="footer">
    <div class="container index-container footer__inner">
        <div class="footer__col footer__col--l">
            <ul class="footer__nav">
                <li class="footer__nav-item">
                    <a href="#" class="footer__nav-link">Помощь</a>
                    <ul class="footer__subnav">
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Оплата и доставка</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Как заказать</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Вопросы и ответы</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Конфиденциальность</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="footer__nav">
                <li class="footer__nav-item">
                    <a href="#" class="footer__nav-link">О компании</a>
                    <ul class="footer__subnav">
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">О нас</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Контакты</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Новости</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Блог</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="footer__nav">
                <li class="footer__nav-item">
                    <a href="#" class="footer__nav-link">Для бизнеса</a>
                    <ul class="footer__subnav">
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Для HoReCa</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Поставщикам</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Оптовикам</a>
                        </li>
                        <li class="footer__subnav-item">
                            <a href="#" class="footer__subnav-link">Бизнес-подарки</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <p class="footer__rights">© 2011–2019 г. Все материалы, размещенные на этом ресурсе (текстовые и графические) — принадлежат компании ООО «Власть Идей». Любое незаконное использование без письменного разрешения запрещено. Официальный сайт специализированного онлайн-магазина 101tea.ru.</p>
        </div>
        <div class="footer__col footer__col--r">
            <div class="footer__subs-wrap">
                <p class="footer__subs-descr">Раз в неделю, пишем о новых акциях и предложениях</p>
                <form action="" class="footer__subs">
                    <input type="email" class="footer__subs-field" placeholder="Электронная почта">
                    <input type="submit" class="footer__subs-btn" value="Подписаться">
                </form>
            </div>
            <div class="footer__contacts-wrap">
                <div>
                    <p class="footer__contacts-title">Контакты</p>
                    <div class="footer__contacts-item">
                        <p class="footer__contacts-subtitle">Вопросы по заказам</p>
                        <a href="mailto:help@101tea.ru" class="footer__contacts-link">help@101tea.ru</a>
                    </div>
                    <div class="footer__contacts-item">
                        <p class="footer__contacts-subtitle">Вопросы сотрудничества</p>
                        <a href="mailto:info@101tea.ru" class="footer__contacts-link">info@101tea.ru</a>
                    </div>
                </div>
                <div>
                    <div class="footer__contacts-item">
                        <p class="footer__contacts-subtitle">Ежедневно с 9 до 21</p>
                        <a href="tel:88002226854" class="footer__contacts-link">8 (800) 222-68-54</a>
                        <a href="tel:88002226854" class="footer__contacts-link">8 (800) 222-68-54</a>
                    </div>
                    <div class="socials footer__socials">
                        <a href="#" class="social social--fb footer__social" target="_blank"></a>
                        <a href="#" class="social social--inst footer__social" target="_blank"></a>
                        <a href="#" class="social social--vk footer__social" target="_blank"></a>
                        <a href="#" class="social social--odk footer__social" target="_blank"></a>
                        <a href="#" class="social social--yt footer__social" target="_blank"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__col footer__col--b">
            <div class="footer__payment-wrap">
                <p class="footer__payment-title">Способ оплаты</p>
                <div class="payment-icons footer__payment-items">
                    <div class="payment-icon payment-icon--visa footer__payment-item"></div>
                    <div class="payment-icon payment-icon--mir footer__payment-item"></div>
                    <div class="payment-icon payment-icon--mc footer__payment-item"></div>
                    <div class="payment-icon payment-icon--wm footer__payment-item"></div>
                    <div class="payment-icon payment-icon--ym footer__payment-item"></div>
                    <div class="payment-icon payment-icon--sber footer__payment-item"></div>
                    <div class="payment-rate footer__payment-rate"></div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>
<div class="popup loc-selector">
    <div class="popup__body loc-selector__body">
        <div class="popup__header loc-selector__header">
            <h2 class="popup__title loc-selector__title">Выбор города</h2>
            <button class="popup__btn-close loc-selector__btn-close js-popup-btn-close"></button>
        </div>
        <div class="popup__main loc-selector__main">
            <p class="loc-selector__descr">Доставляем в 1332 города России, Беларуси и Казахстана</p>
            <form action="" class="loc-selector__search">

                <input type="text" class="loc-selector__search-field" placeholder="Город или населенный пункт">
                <input type="submit" class="loc-selector__search-btn js-btn-form-search">
                <button class="loc-selector__search-btn-reset js-btn-form-reset"></button>
            </form>


            <div class="loc-selector__items">
                <!-- Страна -->
                <p class="loc-selector__country">Россия</p>

                <a href="#" class="loc-selector__item">Москва</a>
                <a href="#" class="loc-selector__item">Казань</a>
                <a href="#" class="loc-selector__item">Уфа</a>

                <a href="#" class="loc-selector__item loc-selector__item--selected">Санкт-Петербург</a>
                <a href="#" class="loc-selector__item">Самара</a>
                <a href="#" class="loc-selector__item">Красноярск</a>

                <a href="#" class="loc-selector__item">Новосибирск</a>
                <a href="#" class="loc-selector__item">Челябинск</a>
                <a href="#" class="loc-selector__item">Пермь</a>

                <a href="#" class="loc-selector__item">Екатеринбург</a>
                <a href="#" class="loc-selector__item">Ярославль</a>
                <a href="#" class="loc-selector__item">Волгоград</a>

                <a href="#" class="loc-selector__item">Нижний новгород</a>
                <a href="#" class="loc-selector__item">Ростов-на-Дону</a>
                <a href="#" class="loc-selector__item">Воронеж</a>

                <!-- END Страна -->

                <!-- Страна -->
                <p class="loc-selector__country">Беларусь</p>

                <a href="#" class="loc-selector__item">Минск</a>
                <a href="#" class="loc-selector__item">Гомель</a>
                <a href="#" class="loc-selector__item">Могилёв</a>

                <a href="#" class="loc-selector__item">Витебск</a>
                <a href="#" class="loc-selector__item">Барановичи</a>
                <a href="#" class="loc-selector__item">Брест</a>

                <a href="#" class="loc-selector__item">Бобруйск</a>
                <a href="#" class="loc-selector__item">Гродно</a>
                <a href="#" class="loc-selector__item">Борисов</a>

                <!-- END Страна -->

                <!-- Страна -->
                <p class="loc-selector__country">Казахстан</p>

                <a href="#" class="loc-selector__item">Алматы</a>
                <a href="#" class="loc-selector__item">Астана</a>
                <a href="#" class="loc-selector__item">Шымкент</a>

                <a href="#" class="loc-selector__item">Павлодар</a>
                <a href="#" class="loc-selector__item">Караганда</a>
                <a href="#" class="loc-selector__item">Актобе</a>

                <a href="#" class="loc-selector__item">Тарас</a>
                <a href="#" class="loc-selector__item">Семей</a>
                <a href="#" class="loc-selector__item">Уральск</a>

                <!-- END Страна -->
            </div>


        </div>
    </div>
</div>

</body>
</html>