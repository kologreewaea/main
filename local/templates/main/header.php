<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Page\Asset;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Подарки</title>
    <?$APPLICATION->ShowHead();?>
</head>
<body>
<div class="body-wrap-scroll">
    <header class="header">
        <div class="header__inner">
            <div class="header__top-wrap">
                <div class="container index-container header__top ">
                    <a href="#" class="header__location js-location">Нижний Новгород</a>
                    <nav class="catalog-nav js-catalog-nav">
                        <div class="catalog-nav__overlay"></div>
                        <div class="catalog-nav__dropdown-container">
                            <div class="catalog-nav__dropdown js-dropdown js-main-dropdown catalog-nav__dropdown--main">
                                <div class="catalog-nav__list-wrap js-dropdown-scroll">
                                    <ul class="catalog-nav__list">
                                        <li class="catalog-nav__item" data-nav-id="500">
                                            <a href="" class="catalog-nav__link">Чай</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="600">
                                            <a href="" class="catalog-nav__link">Посуда для чая</a>
                                        </li>

                                        <li class="catalog-nav__item" data-nav-id="700">
                                            <a href="" class="catalog-nav__link">Чайная церемония</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="800">
                                            <a href="" class="catalog-nav__link">Сладости</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="900">
                                            <a href="" class="catalog-nav__link">Подарки</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="1000">
                                            <a href="" class="catalog-nav__link">Кофе</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="1100">
                                            <a href="" class="catalog-nav__link">Кофейные аксессуары</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="1200">
                                            <a href="" class="catalog-nav__link">Уценённые товары</a>
                                        </li>
                                        <li class="catalog-nav__item" data-nav-id="1300">
                                            <a href="" class="catalog-nav__link">Бренды</a>
                                        </li>
                                    </ul>
                                    <div class="catalog-nav__banner">
                                        <h3 class="catalog-nav__banner-title">
                                            Заголовок в одну или две строки
                                        </h3>
                                        <p class="catalog-nav__banner-descr">
                                            Подзаголовок, описывающий суть предложения
                                        </p>
                                        <a href="" class="catalog-nav__banner-btn">Кнопка</a>
                                    </div>
                                </div>
                            </div>
                            <div class="catalog-nav__dropdown js-dropdown catalog-nav__dropdown--hidden">
                                <div class="catalog-nav__list-wrap js-dropdown-scroll">
                                    <ul class="catalog-nav__list" data-nav-id="500">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Пуэр</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Чёрный чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Чёрный чай с добавками</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Зелёный чай с жасмином</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Красный чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Улун (оолонг) </a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Синий чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Связанный чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Жёлтый чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Белый чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Иван-чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Мате</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ройбуш (ройбос)</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Экзотический чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Фруктовый чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Фито-чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Фито-чай</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Фито-чай</a>
                                        </li>

                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection hidden">
                                            <a href="" class="catalog-nav__link">Каркаде-1</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection hidden">
                                            <a href="" class="catalog-nav__link">Дянь Хун-1</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection hidden">
                                            <a href="" class="catalog-nav__link">Фуцзяньский-1</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="600">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию</a>
                                        </li>

                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="700">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 1</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 1</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="800">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 2</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 2</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="900">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 3</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 3</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="1000">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 4</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 4</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="1100">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 5</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 5</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="1200">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 6</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 6</a>
                                        </li>
                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                    <ul class="catalog-nav__list" data-nav-id="1300">
                                        <li class="catalog-nav__item catalog-nav__item--mob">
                                            <a href="" class="catalog-nav__link-category">Ссылка на категорию 7</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last">
                                            <a href="" class="catalog-nav__link">Ссылка на подкатегорию 7</a>
                                        </li>

                                        <!-- подборки -->
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--title">
                                            <a href="" class="catalog-nav__link">Интересные подборки</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Китайский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Каркаде</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Дянь Хун</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection">
                                            <a href="" class="catalog-nav__link">Фуцзяньский</a>
                                        </li>
                                        <li class="catalog-nav__item catalog-nav__item--last catalog-nav__item-selection catalog-nav__item-selection--more">
                                            <a href="" class="catalog-nav__link catalog-nav__link--more js-nav-more">Смотреть ещё</a>
                                        </li>
                                        <!-- подборки конец-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <ul class="header__nav js-header-nav">
                        <li class="header__nav-item header__nav-item--sale">
                            <a href="" class="header__nav-link">Акции</a>
                        </li>
                        <li class="header__nav-item header__nav-item--pd">
                            <a href="" class="header__nav-link">Оплата и доставка</a>
                        </li>
                        <li class="header__nav-item js-nav-item">
                            <a href="" class="header__nav-link js-nav-link">Для бизнеса</a>

                            <ul class="header__subnav js-subnav">
                                <li class="header__subnav-item header__subnav-item--mob">
                                    <a href="#" class="header__subnav-root">Для бизнеса</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Для HoReCa</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Поставщикам</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Оптовикам</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Бизнес-подарки</a>
                                </li>
                            </ul>
                        </li>
                        <li class="header__nav-item js-nav-item">
                            <a href="" class="header__nav-link js-nav-link">О компании</a>

                            <ul class="header__subnav js-subnav">
                                <li class="header__subnav-item header__subnav-item--mob">
                                    <a href="#" class="header__subnav-root">О компании</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">О нас</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Контакты</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Новости</a>
                                </li>
                                <li class="header__subnav-item">
                                    <a href="#" class="header__subnav-link">Блог</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="header__contacts">
                        <span class="header__contacts-info">Ежедневно с 9 до 21</span>
                        <a href="tel:88002226854" class="header__contact">8 (800) 222-68-54</a>
                        <a href="tel:88002226854" class="header__contact">8 (800) 222-68-54</a>
                        <a href="#" class="header__contact header__contact-callback js-callback">Заказать звонок</a>
                    </div>
                    <div class="socials header__socials">
                        <a href="#" class="social social--fb header__social" target="_blank"></a>
                        <a href="#" class="social social--inst header__social" target="_blank"></a>
                        <a href="#" class="social social--vk header__social" target="_blank"></a>
                        <a href="#" class="social social--odk header__social" target="_blank"></a>
                        <a href="#" class="social social--yt header__social" target="_blank"></a>
                    </div>
                </div>
            </div>
            <div class="header__bottom-wrap">
                <div class="container index-container header__bottom">
                    <div class="header__btn-back js-btn-nav-back"></div>
                    <a href="" class="header__logo"></a>
                    <div class="header__catalog-btn js-btn-catalog">Каталог товаров</div>
                    <form action="" class="header__search">
                        <input type="text" class="header__search-field" placeholder="Поиск по товарам">
                        <input type="submit" class="header__search-btn" value="Искать">
                    </form>
                    <div class="header__search-icon"></div>
                    <div class="header__tabs">
                        <div class="header__tab-wrap">
                            <a href="#" class="header__tab header__tab--index">Главная</a>
                        </div>
                        <div class="header__tab-wrap js-tab-catalog">
                            <a href="#" class="header__tab header__tab--catalog">Каталог</a>
                        </div>
                        <div class="header__tab-wrap">
                            <a href="#" class="header__tab header__tab--fav">Избранное</a>
                        </div>
                        <div class="header__tab-wrap js-tab-profile">
                            <a href="#" class="header__tab header__tab--profile">Профиль</a>
                            <div class="header__tab-body header__profile js-profile">
                                <div class="header__profile-info">
                                    <div class="header__profile-avatar"></div>
                                    <p class="header__profile-name">Артем<span class="header__profile-phone">+7999999999</span></p>
                                </div>
                                <div class="header__profile-links">
                                    <a href="#" class="header__profile-link header__profile-link--points">
                                        Мои баллы <span class="header__profile-score">250</span>
                                    </a>
                                    <a href="#" class="header__profile-link header__profile-link--orders">
                                        Мои заказы
                                    </a>
                                    <a href="#" class="header__profile-link header__profile-link--data">
                                        Личные данные
                                    </a>
                                    <a href="#" class="header__profile-link header__profile-link--logout">
                                        Выход
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header__tab-wrap">
                            <a href="#" class="header__tab header__tab--basket">Корзина</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main>





